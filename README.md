HD44780 Pinout (half-byte mode used)
==========
* E - B6
* RS - B11
* DS4 - B12
* DS5 - B13
* DS6 - B14
* DS7 - B15

1-Wire pinout (parasite power used)
=======================
* DATA - B10
