#ifndef _H_HD44780_H_
#define _H_HD44780_H_

#define HD44780_CMD_DRAM_ADDR 0x80

#define HD44780_CMD_MODE 0x20
#define HD44780_FULL_MODE 0x10
#define HD44780_ROWS_TWO  0x08
#define HD44780_ROWS_ONE  0x00

#define HD44780_FONT_5x8  0x00
#define HD44780_FONT_5x10 0x04

#define HD44780_CMD_DIPLAY 0x08
#define HD44780_DIPLAY_ON  0x04
#define HD44780_DIPLAY_CUR 0x02
#define HD44780_DIPLAY_CUR_BLINK 0x01

#define HD44780_CMD_SHIFT 0x04
#define HD44780_SHIFT_CUR 0x02
#define HD44780_SHIFT_SCR 0x01

#define HD44780_CMD_CLEAR 0x01

void hd44780_init();
void hd44780_clear();
void hd44780_set_xy(uint8_t x, uint8_t y);
void hd44780_out(uint8_t* pstr);
void hd44780_set_mode(uint8_t two_rows, uint8_t font_5x10, uint8_t cursor);
void hd44780_write_command(uint8_t command);
void hd44780_write_data(uint8_t data);

#define HD44780_E_RCC  RCC_GPIOB
#define HD44780_E_BANK GPIOB
#define HD44780_E_PIN  GPIO6

#define HD44780_RS_RCC  RCC_GPIOB
#define HD44780_RS_BANK GPIOB
#define HD44780_RS_PIN  GPIO11

#define HD44780_DS7_RCC  RCC_GPIOB
#define HD44780_DS7_BANK GPIOB
#define HD44780_DS7_PIN  GPIO15

#define HD44780_DS6_RCC  RCC_GPIOB
#define HD44780_DS6_BANK GPIOB
#define HD44780_DS6_PIN  GPIO14

#define HD44780_DS5_RCC  RCC_GPIOB
#define HD44780_DS5_BANK GPIOB
#define HD44780_DS5_PIN  GPIO13

#define HD44780_DS4_RCC  RCC_GPIOB
#define HD44780_DS4_BANK GPIOB
#define HD44780_DS4_PIN  GPIO12

#endif//_H_HD44780_H_