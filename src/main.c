#include "config.h"

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/cortex.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#include "sysclock.h"
#include "hd44780.h"
#include "onewire.h"

//0x30 ->
//Hello
//0x48 0x65  0x6c 0x6c 0x6f

//Ello
//0x45 0x6c 0x6c 0x6f 
//0x23 - #
//0x33 - 3
//0x43 - C
//0x53 - S
//0x63 - c
//0x73 - s

uint8_t pr = 0;
uint8_t inited = 0;
uint8_t ttm = 0;
uint8_t presence = 0;
uint8_t *hex = "0123456789ABCDEF";
uint8_t * spr = "/-\\|";
uint8_t probe_id[8];
uint8_t measure = 0;
int16_t temp = ERR_TEMP_VAL;

void update_status_led(){
   if (!STATUS_LED_DELAY_DOWNCOUNTER){
     gpio_toggle(GPIOC, GPIO13);
     STATUS_LED_DELAY_DOWNCOUNTER = 500;
     if (inited){
         if (!presence){
           hd44780_set_xy(0,0);
           hd44780_out("                ");
           hd44780_set_xy(0,1);
           hd44780_out("Scanning");
           for(uint8_t i=0; i<4; i++){
             hd44780_write_data(i < (ttm>>1) ? '.' : ' ');
           }
        }else{
           hd44780_set_xy(0,0);
           for(uint8_t i=0; i<8; i++){
           hd44780_write_data(hex[probe_id[i]>>4]);
           hd44780_write_data(hex[probe_id[i] & 0xf]);
           }
           hd44780_set_xy(0,1);
           if (temp != ERR_TEMP_VAL){
             uint8_t vv[7];
             int16_t t = temp;
             vv[6] = 0;
             vv[5] = '0' + (t % 10);
             t = t / 10;
             vv[4] = '.';
             vv[3] = '0' + (t % 10);
             t = t / 10;
             vv[2] = '0' + (t % 10);
             t = t / 10;
             vv[1] = '0' + (t % 10);
             t = t / 10;
             if (t < 0) {
               vv[0] = '-';
             }else{
               vv[0] = '+';
             }
             hd44780_out(vv);
             hd44780_out("       ");
           }else{
             hd44780_out("ERR_TEMP_VAL ");
           }
        }
           hd44780_write_data(hex[presence>>4]);
           hd44780_write_data(hex[presence & 0xf]);
           hd44780_write_data(spr[pr & 3]);
       if (ttm < 7){
         ttm++;
       }else{
         ttm=0;
       }
     }
   }
}
int main(void){
  uint8_t ow_data_array[10];

#ifdef USE_HSE_12
  rcc_clock_setup_in_hse_12mhz_out_72mhz();
#else
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
#endif
  rcc_set_usbpre(RCC_CFGR_USBPRE_PLL_CLK_DIV1_5);

  rcc_periph_clock_enable(RCC_AFIO);
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

  onewire_init();

  sysclock_init();

  hd44780_init();
//  hd44780_set_mode(1, 0, 1);
//  hd44780_clear();
  hd44780_out("Hello, world!");
  hd44780_set_xy(0,1);
  hd44780_out("Initializing...");
//  hd44780_set_xy(0,1);
//  hd44780_out("abcdefg");
  inited = 1;
  gpio_set(GPIOC, GPIO13);

  while(1) {
    update_status_led();
    if (!TEMP_PROBE_DOWNCOUNTER){
      pr++;
      if (onewire_reset()){
        presence = ow_scan(probe_id, 1);
      }else{
        presence = 0;
        temp = ERR_TEMP_VAL;
      }
      if (presence){
        if (measure == 0){
          measure = 1;
          ow_data_array[0] = OW_SKIP_ROM;
          ow_data_array[1] = OW_CONVERT_T;
          onewire_reset();
          ow_send_bytes(ow_data_array, 2);

          TEMP_PROBE_DOWNCOUNTER = DS18B20_CONVERSION_PERIOD;
        }else if(measure == 1){
          measure = 0;
          ow_data_array[0] = OW_MATCH_ROM;
          for(int i=0; i<8; i++)
            ow_data_array[i+1] = probe_id[i];
          ow_data_array[9] = OW_READ_SCRATCHPAD;

          onewire_reset();
          ow_send_bytes(ow_data_array, 10);

          ow_receive_bytes(ow_data_array, 9);
  
          temp = gettemp(ow_data_array);
        }
      }else{
        TEMP_PROBE_DOWNCOUNTER = 500;
      }
    }
 }
  return 0;
}