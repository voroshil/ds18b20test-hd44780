#include "config.h"

#include <libopencm3/cm3/cortex.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/nvic.h>
#include "onewire.h"

uint16_t onewire_gotlen = 0;
// array for sending/receiving data

void (*ow_process_resdata)() = 0;

void onewire_init(){
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_USART3);

  gpio_set_mode(GPIO_BANK_USART3_TX, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_USART3_TX); // PB10

  usart_set_baudrate(USART3, 115200);
  usart_set_databits(USART3, 8);
  usart_set_parity(USART3, USART_PARITY_NONE);
  usart_set_stopbits(USART3, USART_STOPBITS_1);
  usart_set_flow_control(USART3, USART_FLOWCONTROL_NONE);
  usart_set_mode(USART3, USART_MODE_TX_RX);
  USART_CR3(USART3) |= USART_CR3_HDSEL;

  usart_enable(USART3);
}
#define OW_R 0xf0
#define OW_1 0xff
#define OW_0 0x00
uint8_t onewire_reset(){
  uint8_t res = 0;
  usart_disable(USART3);
  usart_set_baudrate(USART3, 9600);
  usart_enable(USART3);
  usart_send(USART3, OW_R);
  while ((USART_SR(USART3) & USART_SR_TC) == 0);
  res = usart_recv_blocking(USART3);
  usart_disable(USART3);
  usart_set_baudrate(USART3, 115200);
  usart_enable(USART3);
//  return (res >= 0x10 && res <= 0x90);
  return res != 0xF0;
//11100000
//00010000
//10001111
}
void ow_send_bit(uint8_t v){
  usart_send(USART3, v ? OW_1 : OW_0);
  while ((USART_SR(USART3) & USART_SR_TC) == 0);
  usart_recv_blocking(USART3);
}
uint8_t ow_receive_bit(){
  uint8_t res;
  usart_send(USART3, OW_1);
  while ((USART_SR(USART3) & USART_SR_TC) == 0);
  res = usart_recv_blocking(USART3);
  return res == OW_1;
}

void ow_send_byte(uint8_t v) {
  uint8_t i;

  for (i=0; i<8; i++) {
    ow_send_bit(v & 1);
    v >>= 1;
  }
}

void ow_send_bytes(const uint8_t *buf, uint16_t count) {
  uint16_t i;
  for (i = 0 ; i < count ; i++)
    ow_send_byte(buf[i]);
}

uint8_t ow_receive_byte() {
  uint8_t i;
  uint8_t r = 0;

  for (i = 0; i<8; i++) {
    r >>= 1;
    if (ow_receive_bit()) 
      r |= 0x80;
  }
  return r;
}

void ow_receive_bytes(uint8_t *buf, uint16_t count) {
  uint16_t i;
  for (i = 0 ; i < count ; i++)
    buf[i] = ow_receive_byte();
}
void ow_select(uint8_t rom[8])
{
  uint8_t i;

  ow_send_byte(OW_MATCH_ROM);           // Choose ROM
  for(i=0; i<8; i++) ow_send_byte(rom[i]);
}

void ow_skip()
{
  ow_send_byte(OW_SKIP_ROM);           // Skip ROM
}

/**
 * convert temperature from ow_data_array (scratchpad)
 * in case of error return 200000 (ERR_TEMP_VAL)
 * return value in 1000th degrees centigrade
 * don't forget that bytes in ow_data_array have reverce order!!!
 * so:
 * 0 8 - themperature LSB
 * 1 7 - themperature MSB (all higher bits are sign)
 * 2 6 - T_H
 * 3 5 - T_L
 * 4 4 - B20: Configuration register (only bits 6/5 valid: 9..12 bits resolution); 0xff for S20
 * 5 3 - 0xff (reserved)
 * 6 2 - (reserved for B20); S20: COUNT_REMAIN (0x0c)
 * 7 1 - COUNT PER DEGR (0x10)
 * 8 0 - CRC
 */
int16_t gettemp(uint8_t *ow_data_array){
	// detect DS18S20
	int16_t t = 0;
	uint8_t l,m;
	int8_t  v;
	if(ow_data_array[7] == 0xff) // 0xff can be only if there's no such device or some other error
		return ERR_TEMP_VAL;
	m = ow_data_array[1];
	l = ow_data_array[0];
	if(ow_data_array[4] == 0xff){ // DS18S20
		v = l >> 1 | (m & 0x80); // take signum from MSB
		t = ((int16_t)v) * 10;
		if(l&1) t += 5L; // decimal 0.5
	}
	else{
		v = l>>4 | ((m & 7)<<4);
		if(m&0x80){ // minus
			v |= 0x80;
		}
		t = ((int16_t)v) * 10;
		m = l&0x0f; // add decimal
		t += (int16_t)m; // t = v*10 + l*1.25 -> convert
		if(m > 1) t++; // 1->1, 2->3, 3->4, 4->5, 4->6
		else if(m > 5) t+=2; // 6->8, 7->9
	}
	return t;
}

#if 0
extern param_t *config;
uint8_t last_probe = 0;
uint8_t learning = 1;
void wait_reading(){

  for (uint8_t probe=0; probe<MAX_PROBES; probe++){
    if (GET_CONFIG_PROBE_MODE(probe) == PROBE_MODE_DISABLE){
      continue;
    }

    if (!onewire_reset()){
      EDS->PROBE_BUSY = 0;
      TEMP_PROBE_DOWNCOUNTER_MS = ONEWIRE_TIMEOUT_PERIOD;
      return;
    }
  
    ow_data_array[0] = OW_MATCH_ROM;
    for(int i=0; i<8; i++)
      ow_data_array[i+1] = config->probes[probe].id[i];
    ow_data_array[9] = OW_READ_SCRATCHPAD;

    ow_send_bytes(ow_data_array, 10);

    ow_receive_bytes(ow_data_array, 9);
  
    probe_set(probe, gettemp());
  } 
  ow_process_resdata = 0;
  EDS->PROBE_BUSY = 0;
  TEMP_PROBE_DOWNCOUNTER_MS = ONEWIRE_TIMEOUT_PERIOD;
}
void start_temp_reading(){
  EDS->PROBE_BUSY = 1;
  if(!onewire_reset()){
    TEMP_PROBE_DOWNCOUNTER_MS = ONEWIRE_TIMEOUT_PERIOD;
    EDS->PROBE_BUSY = 0;
    return;
  }

  ow_data_array[0] = OW_SKIP_ROM;
  ow_data_array[1] = OW_CONVERT_T;
  ow_send_bytes(ow_data_array, 2);

  ow_process_resdata = wait_reading;
  TEMP_PROBE_DOWNCOUNTER_MS = DS18B20_CONVERSION_PERIOD;
}

void scan_bus();

void onewire_scan_handler(){
  if (TEMP_PROBE_DOWNCOUNTER_MS){
    return;
  }
  if (EDS->POWER_SAVING){
    return;
  }

  if (EDS->PROBE_BUSY){
    return;
  }

  if ((GET_CONFIG_PROBE_MODE(0) == PROBE_MODE_LEARN) ||
   (GET_CONFIG_PROBE_MODE(1) == PROBE_MODE_LEARN) ||
   (GET_CONFIG_PROBE_MODE(2) == PROBE_MODE_LEARN) ||
   (GET_CONFIG_PROBE_MODE(3) == PROBE_MODE_LEARN)){
      scan_bus();
      start_temp_reading();
  }
}

void onewire_temp_handler(){
  if (TEMP_PROBE_DOWNCOUNTER_MS){
    return;
  }
  TEMP_PROBE_DOWNCOUNTER_MS = ONEWIRE_TIMEOUT_PERIOD;
  if (ow_process_resdata){
    ow_process_resdata();
    return;
  }

  start_temp_reading();
}
unsigned char ROM_NO[8];
uint8_t LastDiscrepancy;
uint8_t LastFamilyDiscrepancy;
uint8_t LastDeviceFlag;

void ow_reset_search()
{
  int i;
  // reset the search state
  LastDiscrepancy = 0;
  LastDeviceFlag = 0;
  LastFamilyDiscrepancy = 0;
  for(i = 7; ; i--)
  {
    ROM_NO[i] = 0;
    if ( i == 0) break;
  }
}

#endif
uint8_t ow_scan(uint8_t *buf, uint8_t num) {

  uint8_t found = 0;
  uint8_t *lastDevice = buf;
  uint8_t *curDevice = buf;
  uint8_t numBit, lastCollision, currentCollision, currentSelection;
  uint8_t ow_buf[2];
  lastCollision = 0;
  while (found < num) {
    numBit = 1;
    currentCollision = 0;

    // посылаем команду на поиск устройств
    onewire_reset();
    ow_send_byte(OW_SEARCH_ROM);

    for (numBit = 1; numBit <= 64; numBit++) {
      // читаем два бита. Основной и комплементарный
      ow_buf[0] = ow_receive_bit();
      ow_buf[1] = ow_receive_bit();

      if (ow_buf[0]) {
        if (ow_buf[1]) {
          // две единицы, где-то провтыкали и заканчиваем поиск
          return found;
        } else {
          // 10 - на данном этапе только 1
          currentSelection = 1;
        }
      } else {
        if (ow_buf[1]) {
          // 01 - на данном этапе только 0
          currentSelection = 0;
        } else {
          // 00 - коллизия
          if (numBit < lastCollision) {
            // идем по дереву, не дошли до развилки
            if (lastDevice[(numBit - 1) >> 3] & 1 << ((numBit - 1) & 0x07)) {
              // (numBit-1)>>3 - номер байта
              // (numBit-1)&0x07 - номер бита в байте
              currentSelection = 1;

              // если пошли по правой ветке, запоминаем номер бита
              if (currentCollision < numBit) {
                currentCollision = numBit;
              }
            } else {
              currentSelection = 0;
            }
          } else {
            if (numBit == lastCollision) {
              currentSelection = 0;
            } else {
              // идем по правой ветке
              currentSelection = 1;

              // если пошли по правой ветке, запоминаем номер бита
              if (currentCollision < numBit) {
                currentCollision = numBit;
              }
            }
          }
        }
      }

      if (currentSelection == 1) {
        curDevice[(numBit - 1) >> 3] |= 1 << ((numBit - 1) & 0x07);
        ow_send_bit(1);
      } else {
        curDevice[(numBit - 1) >> 3] &= ~(1 << ((numBit - 1) & 0x07));
        ow_send_bit(0);
      }
    }
    found++;
    lastDevice = curDevice;
    curDevice += 8;
    if (currentCollision == 0)
      return found;

    lastCollision = currentCollision;
  }

  return found;
}
