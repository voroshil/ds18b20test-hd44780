#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

#include "hd44780.h"
#include "sysclock.h"

void _delay_us(uint16_t us){
  TIM_ARR(TIM2) = us; // Period = us
  TIM_CNT(TIM2) = 0;
  TIM_CR1(TIM2) |= TIM_CR1_CEN;
  while (TIM_CR1(TIM2) & TIM_CR1_CEN);
}


void hd44780_delay(uint16_t delay) {
	volatile unsigned long i;
	for (i=0;i<(delay*10); i++){};
}
void hd44780_delay_ms(uint8_t ms) {
  LCD_DOWNCOUNTER = ms;
  while(LCD_DOWNCOUNTER){
   hd44780_delay(0);
  }
}
void hd44780_write_clock(){
  gpio_set(HD44780_E_BANK, HD44780_E_PIN);
  hd44780_delay(1);
  gpio_clear(HD44780_E_BANK, HD44780_E_PIN);
  hd44780_delay(1);
//  hd44780_delay_ms(200);
//  hd44780_delay_ms(200);
//  hd44780_delay_ms(200);
//  hd44780_delay_ms(200);
//  hd44780_delay_ms(200);
}
void hd44780_write_half_byte(uint8_t byte){
  if (byte & 0x1){
    gpio_set(HD44780_DS4_BANK, HD44780_DS4_PIN);
  }else{
    gpio_clear(HD44780_DS4_BANK, HD44780_DS4_PIN);
  }
  if (byte & 0x2){
    gpio_set(HD44780_DS5_BANK, HD44780_DS5_PIN);
  }else{
    gpio_clear(HD44780_DS5_BANK, HD44780_DS5_PIN);
  }
  if (byte & 0x4){
    gpio_set(HD44780_DS6_BANK, HD44780_DS6_PIN);
  }else{
    gpio_clear(HD44780_DS6_BANK, HD44780_DS6_PIN);
  }
  if (byte & 0x8){
    gpio_set(HD44780_DS7_BANK, HD44780_DS7_PIN);
  }else{
    gpio_clear(HD44780_DS7_BANK, HD44780_DS7_PIN);
  }
}
void hd44780_write_nibble(uint8_t byte){
  hd44780_write_half_byte(byte & 0xf);
  _delay_us(2);
  gpio_set(HD44780_E_BANK, HD44780_E_PIN);
  _delay_us(2);
  gpio_clear(HD44780_E_BANK, HD44780_E_PIN);
  gpio_clear(HD44780_DS4_BANK, HD44780_DS4_PIN);
  gpio_clear(HD44780_DS5_BANK, HD44780_DS5_PIN);
  gpio_clear(HD44780_DS6_BANK, HD44780_DS6_PIN);
  gpio_clear(HD44780_DS7_BANK, HD44780_DS7_PIN);
  _delay_us(40);
}
void hd44780_write_byte(uint8_t byte){
  hd44780_write_nibble(byte >> 4);
  hd44780_write_nibble(byte);
}
void hd44780_write_byte2(uint8_t byte){
  hd44780_write_nibble(byte);
  hd44780_write_nibble(byte >> 4);
}
void hd44780_write_command(uint8_t command){
  gpio_clear(HD44780_RS_BANK, HD44780_RS_PIN);
  hd44780_write_byte(command);
  gpio_set(HD44780_RS_BANK, HD44780_RS_PIN);
}
void hd44780_write_command2(uint8_t command){
  gpio_clear(HD44780_RS_BANK, HD44780_RS_PIN);
  hd44780_write_byte2(command);
  gpio_set(HD44780_RS_BANK, HD44780_RS_PIN);
}
void hd44780_write_data(uint8_t data){
  gpio_set(HD44780_RS_BANK, HD44780_RS_PIN);
  hd44780_write_nibble(data >> 4);
  hd44780_write_nibble(data);
}
void hd44780_enable_half_mode(){
  gpio_clear(HD44780_RS_BANK, HD44780_RS_PIN);
  hd44780_write_nibble(0x2);
}

void hd44780_init(){
  uint8_t i;

  rcc_periph_clock_enable(RCC_TIM2);
  rcc_periph_reset_pulse(RST_TIM2);

//  timer_reset(TIM2);
  timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  timer_set_prescaler(TIM2, (rcc_ahb_frequency / 1000000)-1); // 1MHz
  timer_disable_preload(TIM2);
  timer_one_shot_mode(TIM2);

  rcc_periph_clock_enable(HD44780_RS_RCC);
  rcc_periph_clock_enable(HD44780_E_RCC);
  rcc_periph_clock_enable(HD44780_DS4_RCC);
  rcc_periph_clock_enable(HD44780_DS5_RCC);
  rcc_periph_clock_enable(HD44780_DS6_RCC);
  rcc_periph_clock_enable(HD44780_DS7_RCC);

  gpio_set_mode(HD44780_RS_BANK,  GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, HD44780_RS_PIN);
  gpio_set_mode(HD44780_E_BANK,   GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, HD44780_E_PIN);
  gpio_set_mode(HD44780_DS4_BANK, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, HD44780_DS4_PIN);
  gpio_set_mode(HD44780_DS5_BANK, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, HD44780_DS5_PIN);
  gpio_set_mode(HD44780_DS6_BANK, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, HD44780_DS6_PIN);
  gpio_set_mode(HD44780_DS7_BANK, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, HD44780_DS7_PIN);

  gpio_clear(HD44780_E_BANK, HD44780_E_PIN);
  gpio_clear(HD44780_RS_BANK, HD44780_RS_PIN);

//  hd44780_delay_ms(100);
  hd44780_delay(5000);

  hd44780_write_nibble(0x3);
  hd44780_delay_ms(5);
  hd44780_write_nibble(0x3);
//  hd44780_delay();
//  hd44780_delay();
  hd44780_write_nibble(0x3);
//  hd44780_delay();
//  hd44780_delay();
  hd44780_write_nibble(0x2); // 4-bit mode
//  hd44780_delay();
//  hd44780_delay();
//  hd44780_enable_half_mode();
  hd44780_write_byte(0x28);
//  hd44780_write_command(0x0F);
  hd44780_write_byte(0x08);
  hd44780_write_byte(0x0C);
  hd44780_write_byte(0x06);
  hd44780_write_byte(0x01);
  hd44780_write_byte(0x02);
//  hd44780_delay();
//  hd44780_delay();
//  hd44780_write_command(0x02);
  
#if 0
  for(i=0x40; i<0x5F; i++)
  {
		hd44780_delay(1000);
		hd44780_write_command(i);
		hd44780_delay(1000);
		hd44780_write_data(0);
  }
#endif
  gpio_set(HD44780_RS_BANK, HD44780_RS_PIN);

  hd44780_write_command(0x01);
  hd44780_delay(1000);
  hd44780_write_command(0x80);

}
void hd44780_set_mode(uint8_t two_rows, uint8_t font_5x10, uint8_t cursor){
  uint8_t mode = 0;
  if (two_rows) {
    mode |= HD44780_ROWS_TWO;
  }
  if (font_5x10) {
    mode |= HD44780_FONT_5x10;
  }
  hd44780_write_command(HD44780_CMD_MODE | mode);
  mode = 0;
  if (cursor){
    mode |= HD44780_DIPLAY_CUR | HD44780_DIPLAY_CUR_BLINK;
  }
  hd44780_write_command(HD44780_CMD_DIPLAY | HD44780_DIPLAY_ON | mode);
}
void hd44780_clear(){
  hd44780_write_command(HD44780_CMD_CLEAR);
}
void hd44780_set_xy(uint8_t x, uint8_t y){
  uint8_t addr = x;
  if (y > 1 || x > 0x27){
    return;
  }
  if (y){
    addr += 0x40;
  }
  hd44780_write_command(HD44780_CMD_DRAM_ADDR | addr);
//  hd44780_write_command(0xC2);
}
void hd44780_out(uint8_t* pstr){
  uint8_t *tmp = pstr;
  while(*tmp){
    hd44780_write_data(*tmp);
    tmp++;
  }
}
